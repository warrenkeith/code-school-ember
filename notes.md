`var App = Ember.Application.create({
    LOG_TRANSITIONS: true
});`

`<script type="text/x-handlebars" data-template-name="hello"></script>`

DEFAULTS!
- The application template is shown for every page by default.
- If Ember reaches an {{outlet}}, by default it will look to find a template named 'index' and render that in place of the outlet.

###Set up an APPLICATION
`var App = Ember.Application.create({
    LOG_TRANSITIONS: true
});`

###Set up a ROUTE
`App.Router.map(function(){
    this.route('about', {path: '/aboutus'});
});`

###Creating LINKS
`{{#link-to 'index' tagName="li"}}Homepage{{/link-to}}`

###Tag PROPERTIES need to be BOUND differently
`<img {{bind-attr src='logo'}} alt='Logo'>`

###Setting up a CONTROLLER
`App.IndexController = Ember.Controller.extend({
   productsCount: 6,
   logo: 'images/logo.png',
   time: function(){
       return (new Date()).toDateString();
   }.property();
});`

###Create a RESOURCE ROUTE
`App.Router.map(function(){
    this.resource('products');
});`

###Feed some data into a RESOURCE ROUTE
`App.ProductsRoute = Ember.Route.extend({
  model: function() {
    return App.PRODUCTS;
  }
});`

###Set up a DYNAMIC ROUTE
`this.resource('product', {path: '/products/:title'});`

`App.ProductRoute = Ember.Route.extend({
  model: function(params) {
    return App.PRODUCTS.findBy('title', params.title);
  }
});`

###LINK TO a DYNAMIC ROUTE
`{{#link-to 'product' this tagName='button' class='btn btn-success'}}Buy for ${{price}}{{/link-to}}`
`{{#link-to 'product' this}}{{title}}{{/link-to}}`

---

###NESTED ROUTES
`App.Router.map(function(){
  this.route('about');
  this.resource('products', function(){
    this.resource('product', {path: '/title'});
    });
});`

Use an `{{outlet}}` to display the content from the nested controller.

---

###Create a model for data (a fixture being data loaded from memory)
`App.Product = DS.Model.extend({});`


---

##Ember Data ADAPTERS
`App.ApplicationAdapter = DS.RESTAdapter.extend();`
`App.ApplicationAdapter = DS.FixtureAdapter.extend();`

The above just seems to make it available to the application without parameters!

---

* _Create a new Model for the data_
  `App.Contact = DS.Model.extend({});`

* _Create the attributes that you're using from the array/data_
  `App.Contact = DS.Model.extend({
      name: DS.attr('string'),
      about: DS.attr('string'),
      avatar: DS.attr('string')
    });`

* _all data must have unique ID!_

* _Change the dynamic segment of the route to use an identifier that Ember Data expects_
  `this.resource('products', function(){
      this.resource('product', {path: '/:id'});
    });`

* _Offer up the new data store to the routes_

    Single:
  `App.ContactRoute = Ember.Route.extend({
    model: function(params) {
      return this.store.find('contact', params.id);
    }
  })`

    Listing:
  `App.ContactsRoute = Ember.Route.extend({
    model: function(params) {
      return this.store.findAll('contact');
    }
  })`
