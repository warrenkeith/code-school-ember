var App = Ember.Application.create({
    LOG_TRANSITIONS: true
});

App.Router.map(function(){
    this.route('about', {path: '/aboutus'});
    this.route('monkey');
    this.resource('products', function(){
      this.resource('product', {path: '/:id'});
    });

});

App.ProductsRoute = Ember.Route.extend({
  model: function() {
    return this.store.findAll('product');
  }
});

App.ProductRoute = Ember.Route.extend({
  model: function(params) {
    return this.store.find('product', params.id);
  }
});

App.ApplicationAdapter = DS.FixtureAdapter.extend();

App.Product = DS.Model.extend({
  title: DS.attr(),
  price: DS.attr(),
  desc: DS.attr(),
  sale: DS.attr(),
  image: DS.attr()
});

App.Product.FIXTURES = [
  {
    "id": 1,
    "title": "Flint",
    "price": 19.99,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo ipsum illo, consequuntur tempore quaerat consectetur atque quisquam similique adipisci, accusantium autem dicta nulla omnis explicabo excepturi ipsam ducimus amet numquam.",
    "isOnSale": true,
    "image": "images/products/flint.png"
  },
  {
    "id": 2,
    "title": "Kindling",
    "price": 9.99,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo ipsum illo, consequuntur tempore quaerat consectetur atque quisquam similique adipisci, accusantium autem dicta nulla omnis explicabo excepturi ipsam ducimus amet numquam.",
    "isOnSale": false,
    "image": "images/products/kindling.png"
  }
]
